import pandas as pd
from docx2pdf import convert
from docx import Document
import os
import time


def Teilnehmerliste_Ablesen():
    Teilnehmerliste = pd.read_excel('Teilnehmerliste.xlsx', sheet_name='Teilnehmerliste')
    Teilnehmerliste['Datum'] = Teilnehmerliste['Datum'].apply(lambda x: x.strftime('%d.%m.%Y'))
    Teilnehmerliste['Name'] = Teilnehmerliste['Vorname'] + ' ' + Teilnehmerliste['Nachname']
    return Teilnehmerliste


def Bescheinigung_erstellen(Infor: dict):
    # Parameter vom Info:
    #     Name
    #     Tage
    #     Exkusionsname
    #     Exkursionsziel
    #     Exkursionsleitung
    #     Datum
    Filename = 'Teilnahmebescheinigung_' + Infor['<Name>'].replace(' ', '_') + '.docx'
    Bescheinigung = Document('Teilnahmebescheinigung Exkursion.docx')
    Bescheinigung = check_and_change(Bescheinigung, Infor)
    Bescheinigung.save(Filename)
    time.sleep(0.2)
    convert(Filename)
    time.sleep(0.2)
    os.remove(Filename)


def check_and_change(document, replace_dict):
    """
    遍历word中的所有 paragraphs，在每一段中发现含有key 的内容，就替换为 value 。
    （key 和 value 都是replace_dict中的键值对。）
    """
    for para in document.paragraphs:
        for i in range(len(para.runs)):
            for key, value in replace_dict.items():
                if key in para.runs[i].text:
                    # print(key + "-->" + value)
                    para.runs[i].text = para.runs[i].text.replace(key, value)
    return document


if __name__ == "__main__":
    Liste = Teilnehmerliste_Ablesen()
    Infor = {'<Name>': 'Test',
             '<Tage>': '0',
             'Exkursionsname': 'Test',
             '<Exkursionsziel>': 'Test',
             '<Exkursionsleitung>': 'Test',
             '<Datum>': 'Test'}
    for i in range(len(Liste)):
        Infor['<Name>'] = str(Liste[i:i + 1]['Name'].values[0])
        Infor['<Tage>'] = str(Liste[i:i + 1]['Tage'].values[0])
        Infor['Exkursionsname'] = str(Liste[i:i + 1]['Exkursionsname'].values[0])
        Infor['<Exkursionsziel>'] = str(Liste[i:i + 1]['Exkursionsziel'].values[0])
        Infor['<Exkursionsleitung>'] = str(Liste[i:i + 1]['Exkursionsleitung'].values[0])
        Infor['<Datum>'] = str(Liste[i:i + 1]['Datum'].values[0])
        print(f'Bescheinigung für {Infor["<Name>"]} ist in Bearbeitung')
        Bescheinigung_erstellen(Infor)
